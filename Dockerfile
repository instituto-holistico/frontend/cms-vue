FROM node:lts-alpine as build-stage
LABEL maintainer 'Jonathas Rodrigues jrc1@ifal.edu.br'

WORKDIR /app
COPY package*.json ./
RUN yarn install --production=true
COPY . .
RUN yarn global add @vue/cli
RUN yarn global add @vue/cli-service
RUN vue add @vue/babel
RUN yarn build

# production stage
FROM nginx:stable-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
#ADD ./nginx/default.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
