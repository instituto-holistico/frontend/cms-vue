import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: () => import('./views/Login.vue'),
    meta: {
      public: true,
    },
  },
  /*
  {
    path: '/doacoes',
    name: 'Ver doações',
    component: () => import('./views/Donations.vue'),
    meta: {
      requiresAuth: process.env.NODE_ENV === 'production',
      public: !(process.env.NODE_ENV === 'production'),
    },
  },
  {
    path: '/notificacoes',
    name: 'Publicar notificação',
    component: () => import('./views/Notifications.vue'),
    meta: {
      requiresAuth: process.env.NODE_ENV === 'production',
      public: !(process.env.NODE_ENV === 'production'),
    },
  },
  */
  {
    path: '/noticias/adicionar/:id?',
    name: 'Publicar notícia',
    component: () => import('./views/AddNews.vue'),
    meta: {
      requiresAuth: process.env.NODE_ENV === 'production',
      public: !(process.env.NODE_ENV === 'production'),
    },
  },
  {
    path: '/secoes/adicionar/:id?',
    name: 'Adicionar seção',
    component: () => import('./views/AddSections.vue'),
    meta: {
      requiresAuth: process.env.NODE_ENV === 'production',
      public: !(process.env.NODE_ENV === 'production'),
    },
  },
  {
    path: '/servicos/adicionar/:id?',
    name: 'Adicionar serviços',
    component: () => import('./views/AddServices.vue'),
    meta: {
      requiresAuth: process.env.NODE_ENV === 'production',
      public: !(process.env.NODE_ENV === 'production'),
    },
  },
  {
    path: '/parceiros/adicionar/:id?',
    name: 'Adicionar parceiro',
    component: () => import('./views/AddPartner.vue'),
    meta: {
      requiresAuth: process.env.NODE_ENV === 'production',
      public: !(process.env.NODE_ENV === 'production'),
    },
  },
  {
    path: '/secoes',
    name: 'Ver seções',
    component: () => import('./views/Sections.vue'),
    meta: {
      requiresAuth: process.env.NODE_ENV === 'production',
      public: !(process.env.NODE_ENV === 'production'),
    },
  },
  {
    path: '/servicos',
    name: 'Ver serviços da instituição',
    component: () => import('./views/Services.vue'),
    meta: {
      requiresAuth: process.env.NODE_ENV === 'production',
      public: !(process.env.NODE_ENV === 'production'),
    },
  },
  {
    path: '/noticias',
    name: 'Ver notícias',
    component: () => import('./views/News.vue'),
    meta: {
      requiresAuth: process.env.NODE_ENV === 'production',
      public: !(process.env.NODE_ENV === 'production'),
    },
  },
  {
    path: '/parceiros',
    name: 'Ver parceiros',
    component: () => import('./views/Partner.vue'),
    meta: {
      requiresAuth: process.env.NODE_ENV === 'production',
      public: !(process.env.NODE_ENV === 'production'),
    },
  },
  {
    path: '/editar-logotipo',
    name: 'Editar logotipo',
    component: () => import('./views/EditLogo.vue'),
    meta: {
      requiresAuth: process.env.NODE_ENV === 'production',
      public: !(process.env.NODE_ENV === 'production'),
    },
  },
  {
    path: '/ajuda',
    name: 'Documentação',
    component: () => import('./views/Docs.vue'),
    meta: {
      requiresAuth: process.env.NODE_ENV === 'production',
      public: !(process.env.NODE_ENV === 'production'),
    },
  },
  {
    path: '*',
    redirect: '/login',
  },
];

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  const TOKEN_KEY = '@jw-ih-t';

  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!localStorage.getItem(TOKEN_KEY)) {
      next({
        path: '/login',
        params: { nextUrl: to.fullPath },
      });
    } else {
      next();
    }
  } else if (to.matched.some(record => record.meta.public)) {
    if (to.fullPath === '/login') {
      if (localStorage.getItem(TOKEN_KEY)) {
        next({
          path: '/secoes',
        });
      } else {
        next();
      }
    } else {
      next();
    }
  }
});

export default router;
