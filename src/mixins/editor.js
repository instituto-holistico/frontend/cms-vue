const editorContent = {
  data() {
    return {
      content: '',
      clearContent: false,
    };
  },
  methods: {
    handleContentChanged(data) {
      this.content = data;
    },
    clearEditorContent() {
      this.clearContent = true;
    },
  },
};

export default editorContent;
