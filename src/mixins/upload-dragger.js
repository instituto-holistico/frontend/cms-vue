const uploadDragger = {
  data() {
    return {
      fileList: [],
    };
  },
  methods: {
    handleRemove(file) {
      const index = this.fileList.indexOf(file);
      const newFileList = this.fileList.slice();
      newFileList.splice(index, 1);
      this.fileList = newFileList;
    },
    beforeUpload(file) {
      // this.fileList = [...this.fileList, file];
      this.fileList = [file];
      return false;
    },
    checkFileList() {
      return this.fileList.length > 0;
    },
  },
};

export default uploadDragger;
