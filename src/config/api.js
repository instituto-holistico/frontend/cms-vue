/* eslint-disable no-param-reassign */
import axios from 'axios';

const baseURL = 'http://api.institutoholistico.com.br';

const api = axios.create({
  baseURL,
  headers: {
    Accept: 'application/json',
    'Content-type': 'multipart/form-data',
  },
});

const KEY_LOCAL_STORAGE = '@jw-ih-t';

api.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem(KEY_LOCAL_STORAGE);

    if (token) {
      config.headers['x-access-token'] = `${token}`;
    }

    return config;
  },
  error => Promise.reject(error),
);

export default api;
