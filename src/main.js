import Vue from 'vue';
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';

import api from './config/api';
import router from './router';
import store from './store';
import VPage from './components/VPage.vue';
import NoData from './components/NoData.vue';
import PageLoading from './components/PageLoading.vue';

import App from './App.vue';

Vue.use(Antd);
Vue.component('page', VPage);
Vue.component('no-data', NoData);
Vue.component('page-loading', PageLoading);

Vue.config.productionTip = false;
Vue.prototype.$http = api;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
