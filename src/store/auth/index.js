import getters from './getters';
import mutations from './mutations';
import actions from './actions';

export default {
  namespaced: true,
  state: {
    accessToken: null,
    loggingIn: false,
    loginError: null,
  },
  getters,
  mutations,
  actions,
};
