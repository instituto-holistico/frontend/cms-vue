import axios from 'axios';
// import api from '../../config/api';
import router from '../../router';

const KEY_LOCAL_STORAGE = '@jw-ih-t';
const LOGIN_ENDPOINT = 'http://api.institutoholistico.com.br/sessions';
const FIRST_PAGE_ROUTE = '/secoes';

export default {
  login({ commit }, loginData) {
    commit('loginStart');

    axios({
      method: 'POST',
      url: LOGIN_ENDPOINT,
      headers: {
        'Content-type': 'application/json; charset=utf-8',
        Accept: 'application/json; charset=utf-8',
      },
      data: loginData,
    })
      .then((response) => {
        localStorage.setItem(KEY_LOCAL_STORAGE, response.data.token);
        commit('loginStop', null);
        commit('updateAccessToken', response.data.token);
        router.push(FIRST_PAGE_ROUTE);
      })
      .catch((error) => {
        commit('loginStop', error);
        commit('updateAccessToken', null);
      });
  },
  fetchAccessToken({ commit }) {
    commit('updateAccessToken', localStorage.getItem(KEY_LOCAL_STORAGE));
  },
  logout({ commit }) {
    localStorage.removeItem(KEY_LOCAL_STORAGE);
    commit('logout');
    router.push('/login');
  },
};
